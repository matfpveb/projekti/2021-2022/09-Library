import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegisterFormComponent } from './components/register-form/register-form.component';
import { StartPageComponent } from './components/start-page/start-page.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { UserInfoComponent } from './components/user-info/user-info.component';
import { BookLibraryComponent } from './components/book-library/book-library.component';
import { BookListComponent } from './components/book-list/book-list.component';
import { BookCardComponent } from './components/book-card/book-card.component';
import { UserInfoWindowComponent } from './components/user-info-window/user-info-window.component';
import { BookLibraryWindowComponent } from './components/book-library-window/book-library-window.component';
import { LogoutComponent } from './components/logout/logout.component';
import { BookPageComponent } from './components/book-page/book-page.component';
import { BookPageWindowComponent } from './components/book-page-window/book-page-window.component';
import { ReviewFormComponent } from './components/review-form/review-form.component';
import { ReviewComponent } from './components/review/review.component';
import { ReviewsListComponent } from './components/reviews-list/reviews-list.component';
import { ReviewsWindowComponent } from './components/reviews-window/reviews-window.component';
import { AddBookWindowComponent } from './components/add-book-window/add-book-window.component';
import { AddBookComponent } from './components/add-book/add-book.component';
import { UserReservationsWindowComponent } from './components/user-reservations-window/user-reservations-window.component';
import { UserReservationsListComponent } from './components/user-reservations-list/user-reservations-list.component';
import { UserReservationComponent } from './components/user-reservation/user-reservation.component';
import { AdminReservationsWindowComponent } from './components/admin-reservations-window/admin-reservations-window.component';
import { AdminReservationsListComponent } from './components/admin-reservations-list/admin-reservations-list.component';
import { AdminReservationComponent } from './components/admin-reservation/admin-reservation.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginFormComponent,
    RegisterFormComponent,
    StartPageComponent,
    SidebarComponent,
    UserInfoComponent,
    BookLibraryComponent,
    BookListComponent,
    BookCardComponent,
    UserInfoWindowComponent,
    BookLibraryWindowComponent,
    LogoutComponent,
    BookPageComponent,
    BookPageWindowComponent,
    ReviewFormComponent,
    ReviewComponent,
    ReviewsListComponent,
    ReviewsWindowComponent,
    AddBookWindowComponent,
    AddBookComponent,
    UserReservationsWindowComponent,
    UserReservationsListComponent,
    UserReservationComponent,
    AdminReservationsWindowComponent,
    AdminReservationsListComponent,
    AdminReservationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
