import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Review } from '../models/review.model';
import { ReviewPagination } from './models/review-pagination';


@Injectable({
  providedIn: 'root'
})
export class ReviewsService {

  private static readonly urls = {
    getUrl: "http://localhost:5000/api/reviews",
    postUrl: "http://localhost:5000/api/reviews/"
  }

  constructor(private http: HttpClient) { }

  public getReviews(page: number = 1, limit: number = 6): Observable<ReviewPagination>{

    const params: HttpParams = new HttpParams().append('page', page).append('limit', limit);

    return this.http.get<ReviewPagination>(ReviewsService.urls.getUrl, {params});
  }

  public postReview(title: string, text: string, user:string, book:string, date:Date): Observable<Review> {
    
    const body = {
      title,
      text,
      user,
      book,
      date
    }

    return this.http.post<Review>(ReviewsService.urls.postUrl,body);
  }

}
