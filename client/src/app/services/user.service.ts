import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, tap } from 'rxjs';
import { User } from '../models/user.model';
import { AuthService } from './auth.service';
import { JwtService } from './jwt.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private static readonly urls = {
    patchPasswordUrl: "http://localhost:5000/api/users/password",
    patchEmailUrl: "http://localhost:5000/api/users/email",
    patchUserProfileImage: "http://localhost:5000/api/users/profile-image"
  };

  constructor(private http: HttpClient, private auth: AuthService, private jwt: JwtService) { }

  public patchUserPassword(oldPassword: string, newPassword: string): Observable<User> {

    const body = {oldPassword, newPassword};
    const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwt.getToken()}`);

    const obs: Observable<{token: string}> = this.http.patch<{token: string}>(UserService.urls.patchPasswordUrl,body,{headers});

    return obs.pipe(
      tap((response: {token: string}) => this.jwt.setToken(response.token)),
      map((response: {token: string}) => this.auth.sendUserDataIfExists()!)
    );
  }       

  public patchUserEmail(email: string, password: string): Observable<User> {

    const body = {email, password};
    const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwt.getToken()}`);

    const obs: Observable<{token: string}> = this.http.patch<{token: string}>(UserService.urls.patchEmailUrl, body, {headers});

    return obs.pipe(
      tap((response: {token: string}) => this.jwt.setToken(response.token)),
      map((response: {token: string}) => this.auth.sendUserDataIfExists()!)
    );
  }

  public patchUserProfileImage(file: File): Observable<User> {

    const body = new FormData();
    body.append("file", file);
    const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwt.getToken()}`);

    const obs: Observable<{token: string}> = this.http.patch<{token: string}>(UserService.urls.patchUserProfileImage, body, {headers});

    return obs.pipe(
      tap((response: {token: string}) => this.jwt.setToken(response.token)),
      map((response: {token: string}) => this.auth.sendUserDataIfExists()!)
    );

  }
}
