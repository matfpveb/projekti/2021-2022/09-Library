import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ReservationPagination } from "src/app/services/models/reservation-pagination";
import { Reservation } from '../models/reservation.model';

@Injectable({
  providedIn: 'root'
})
export class ReservationsService {

  private static readonly urls = {
    getAllReservationsUrl: "http://localhost:5000/api/reservations/",
    getAllUserReservationsUrl: "http://localhost:5000/api/reservations/username/",
    changeReservationStatusUrl: "http://localhost:5000/api/reservations/status"
  }

  constructor(private http: HttpClient) { }

  public getAllReservations(page: number = 1, limit: number = 6, sort_by: string): Observable<ReservationPagination>{

    const params: HttpParams = new HttpParams().append('page', page).append('limit', limit).append('sort_by', sort_by);

    return this.http.get<ReservationPagination>(ReservationsService.urls.getAllReservationsUrl, {params});
  }

  public getAllUserReservations(username: string, page: number = 1, limit: number = 6, sort_by: string): Observable<ReservationPagination>{

    const params: HttpParams = new HttpParams().append('username', username).append('page', page).append('limit', limit).append('sort_by', sort_by);

    return this.http.get<ReservationPagination>(ReservationsService.urls.getAllUserReservationsUrl, {params});
  }

  public changeReservationStatus(reservationId: string, newStatus: string): Observable<ReservationPagination>{

    const body = {'_id': reservationId, 'status': newStatus};

    return this.http.put<ReservationPagination>(ReservationsService.urls.changeReservationStatusUrl, body);
  }
}
