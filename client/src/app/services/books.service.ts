import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, Subscription, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Book } from '../models/book.model';
import { BookPagination } from './models/book-pagination';
import { AuthService } from './auth.service';
import { JwtService } from './jwt.service';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  public subject: Subject<BookPagination> = new Subject();
  public subscription: Subscription = new Subscription();

  private readonly urls = {
    'allBooksUrl': "http://localhost:5000/api/books/",
    'searchUrl': "http://localhost:5000/api/books/search/",
    'genreUrl': "http://localhost:5000/api/books/genre/",
    'searchGenreUrl': "http://localhost:5000/api/books/search/genre/",
    'rating': "http://localhost:5000/api/books/rating/",
    'addBook': "http://localhost:5000/api/books/"
  };

  constructor(private http: HttpClient, 
              private jwt: JwtService) { }

  public getAllBooks(page: number, limit: number): Subject<BookPagination> {

    const params: HttpParams = new HttpParams()
    .append('page', page.toString())
    .append('limit', limit.toString());

    return this.createObservableStream(params, this.urls.allBooksUrl);
  }

  public getBooksBySearchedValue(searchBy: string, searchValue: string, page: number = 1, limit: number = 8): Subject<BookPagination> {
    
    const params: HttpParams = new HttpParams()
    .append('searchBy', searchBy)
    .append('searchValue', searchValue)
    .append('page', page.toString())
    .append('limit', limit.toString());

    return this.createObservableStream(params, this.urls.searchUrl);
  }

  public getBooksByGenre(genre: string, page: number = 1, limit: number = 8): Subject<BookPagination> {

    const params: HttpParams = new HttpParams()
    .append('genre', genre)
    .append('page', page.toString())
    .append('limit', limit.toString());

    return this.createObservableStream(params, this.urls.genreUrl);
  }

  public getBooksBySearchedValueAndGenre(searchBy: string, searchValue: string, genre: string, page: number = 1, limit: number = 8): Subject<BookPagination> {
    
    const params: HttpParams = new HttpParams()
    .append('searchBy', searchBy)
    .append('searchValue', searchValue)
    .append('genre', genre)
    .append('page', page.toString())
    .append('limit', limit.toString());

    return this.createObservableStream(params, this.urls.searchGenreUrl);
  }

  private createObservableStream(params: HttpParams, url: string): Subject<BookPagination> {
    const obs: Observable<BookPagination> = this.http.get<BookPagination>(url, {params});

    if (this.subscription) {
      this.subscription.unsubscribe();
    }

    this.subscription = obs.subscribe((paginationData: BookPagination) => {
      this.subject.next(paginationData);
    });

    return this.subject;
  }

  public getBookById(bookId: string): Observable<Book> {

    return this.http.get<Book>(`http://localhost:5000/api/books/${bookId}`);

  }

  public updateBookRating(id: string, newRating: number): Observable<Book> {
    const body = {id, newRating};

    return this.http.put<Book>(this.urls.rating, body);
  }

  public addBook(title: string, author: string, genre: string, number_of_pages: number, rating: number, publisher: string,description: string,books_available: number): Observable<Book> {
    
    const body = {
      title, 
      author, 
      genre, 
      number_of_pages, 
      rating, 
      publisher,
      description,
      books_available
    }
    
    return this.http.post<Book>(this.urls.addBook, body);
  }

}
