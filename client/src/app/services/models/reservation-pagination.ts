import { Reservation } from 'src/app/models/reservation.model';

export interface ReservationPagination {
    docs: Reservation[];
    totalDocs: number;
    limit: number;
    totalPages: number;
    page: number;
    pagingCounter: number;
    hasPrevPage: boolean;
    hasNextPage: boolean;
    prevPage: number | null;
    nextPage: number | null;
}