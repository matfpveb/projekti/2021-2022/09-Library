import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, Subject, tap } from 'rxjs';
import { IJwtTokenData } from '../models/jwt-token-data';
import { User } from '../models/user.model';
import { JwtService } from './jwt.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  isLogged: boolean = false;
  isAdmin: boolean = false;

  private readonly urls = {
    registerUrl: "http://localhost:5000/api/users/register",
    loginUrl: "http://localhost:5000/api/users/login"
  }

  private readonly userSubject: Subject<User | null> = new Subject<User | null>();
  public readonly user: Observable<User | null> = this.userSubject.asObservable(); 

  constructor(private http: HttpClient, private jwt: JwtService) { }

  public registerUser(username: string, password: string, email: string, name: string, lastName: string): Observable<User | null> {

    const body = {
      username,
      password,
      email,
      name,
      lastName
    }

    const obs: Observable<{token: string}> = this.http.post<{token: string}>(this.urls.registerUrl, body);
 
    return obs.pipe(
      tap((response: {token: string}) => this.jwt.setToken(response.token)),
      map((response: {token: string}) => this.sendUserDataIfExists())
    );
  }

  public loginUser(username: string, password: string): Observable<User | null> {

    const obs: Observable<{token: string}> =  this.http.post<{token: string}>(this.urls.loginUrl,{username, password})

    return obs.pipe(
      tap((response: {token: string}) => this.jwt.setToken(response.token)),
      map((response: {token: string}) => this.sendUserDataIfExists())
    );
  }

  public sendUserDataIfExists(): User | null{
    const payload: IJwtTokenData | null= this.jwt.getDataFromToken();

    if (!payload) {
      return null;
    }

    this.isLogged = true;
    
    if (payload.username === 'admin') {
      this.isAdmin = true;
    }

    const newUser: User =  new User(payload.id,payload.username,payload.password,payload.email,payload.name,payload.lastName,payload.imgUrl);
    this.userSubject.next(newUser);
    return newUser;
  }

  public logout(): void {
    this.jwt.removeToken();
    this.userSubject.next(null);

    this.isLogged = false;
    this.isAdmin = false;
  }
}
