import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddBookWindowComponent } from './components/add-book-window/add-book-window.component';
import { AdminReservationsWindowComponent } from './components/admin-reservations-window/admin-reservations-window.component';
import { BookLibraryWindowComponent } from './components/book-library-window/book-library-window.component';
import { BookPageWindowComponent } from './components/book-page-window/book-page-window.component';
import { BookPageComponent } from './components/book-page/book-page.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { LogoutComponent } from './components/logout/logout.component';
import { RegisterFormComponent } from './components/register-form/register-form.component';
import { ReviewsWindowComponent } from './components/reviews-window/reviews-window.component';
import { UserInfoWindowComponent } from './components/user-info-window/user-info-window.component';
import { UserReservationsWindowComponent } from './components/user-reservations-window/user-reservations-window.component';
import { AdminGuard } from './guards/admin.guard';
import { UserAuthenticatedGuard } from './guards/user-authenticated.guard';
import { UserGuard } from './guards/user.guard';

const routes: Routes = [
  {path: '', component: BookLibraryWindowComponent, canActivate: [UserAuthenticatedGuard, UserGuard]},
  {path: 'login', component: LoginFormComponent},
  {path: 'register', component: RegisterFormComponent},
  {path: 'settings', component: UserInfoWindowComponent, canActivate: [UserAuthenticatedGuard, UserGuard]},
  {path: 'logout', component: LogoutComponent, canActivate: [UserAuthenticatedGuard]},
  {path: 'books/:bookId', component: BookPageWindowComponent, canActivate: [UserAuthenticatedGuard, UserGuard]},
  {path: 'reviews/:page', component: ReviewsWindowComponent, canActivate: [UserAuthenticatedGuard, UserGuard]},
  {path: 'addBook', component: AddBookWindowComponent, canActivate: [UserAuthenticatedGuard, AdminGuard]},
  {path: 'user/reservations/:page', component: UserReservationsWindowComponent, canActivate: [UserAuthenticatedGuard, UserGuard]},
  {path: 'admin/reservations/:page', component: AdminReservationsWindowComponent, canActivate: [UserAuthenticatedGuard, AdminGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
