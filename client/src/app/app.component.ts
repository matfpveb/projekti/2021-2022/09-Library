import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { User } from './models/user.model';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy{
  title = 'client';

  user: User | null = null;
  sub: Subscription = new Subscription();

  constructor(private auth: AuthService) {
    this.sub = this.auth.user.subscribe((user: User | null) => {
      this.user = user;
    });

    this.user = this.auth.sendUserDataIfExists();
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

}
