import { Book } from "./book.model";
import { User } from "./user.model";

export class Review {
    constructor(
                public _id: string,
                public title: string, 
                public text: string, 
                public user: User,
                public book: Book,
                public date: string,
                public __v: number) {}
}