import { Book } from "./book.model";

export class Reservation {
    constructor(public _id: string,
                public username: string,
                public bookId: string,
                public book: Book,
                public status: string,
                public date: string)
    {

    }
}