export class User {
    constructor(public _id: string, 
                public username: string, 
                public password: string, 
                public email: string, 
                public name: string, 
                public lastName: string,
                public imgUrl: string) 
    {

    }
    getImageUrl(): string {
        return 'http://localhost:5000/' + this.imgUrl;
    }
}