export class Book {
    constructor(public _id: string,
                public title: string,
                public author: string, 
                public genre: string, 
                public number_of_pages: number, 
                public rating: number, 
                public publisher: string,
                public description: String,
                public books_available: number,
                public __v: number) 
    {

    }
}