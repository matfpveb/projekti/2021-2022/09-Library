export interface IJwtTokenData {
    id: string,
    username: string,
    password: string,
    email: string,
    name: string,
    lastName: string,
    imgUrl: string
}