import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit,OnDestroy {

  registerForm: FormGroup;
  sub: Subscription = new Subscription();

  constructor(private auth: AuthService,
              private router: Router) { 

    this.registerForm = new FormGroup({
      username: new FormControl('', [Validators.required,Validators.pattern(new RegExp('[0-9a-zA-Z_-]{4,}')), Validators.minLength(4)]),
      password: new FormControl('', [Validators.required, Validators.pattern(new RegExp('[0-9a-zA-Z_-]{8,}')), Validators.minLength(8)]),
      confirmPassword: new FormControl('', [Validators.required, Validators.pattern(new RegExp('[0-9a-zA-Z_-]{8,}'))]),
      email: new FormControl('', [Validators.required, Validators.email]),
      name: new FormControl('', [Validators.required, Validators.pattern(new RegExp('[a-zA-Z]{2,}'))]),
      lastName: new FormControl('', [Validators.required, Validators.pattern(new RegExp('[a-zA-Z]{2,}'))])
    });

  }

  ngOnInit(): void {
  
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  usernameHasErrors(): boolean {

    const errors: ValidationErrors | null | undefined = this.registerForm.get("username")?.errors;

    return errors !== null;
  }

  usernameErrors(): string[] {

    const errorMessages: string[] = [];

    const errors: ValidationErrors | null | undefined = this.registerForm.get("username")?.errors;

    if (!errors) {
      return [];
    }

    if (errors['required']) {
      errorMessages.push("Username is required.");
    }

    if (errors['pattern']) {
      errorMessages.push("Username can cotain letters, numbers, dash and underscore.");
    }

    if (errors['minlength']){
      errorMessages.push("Username must be at least four characters long.");
    }

    return errorMessages;
  }

  passwordHasErrors(): boolean {

    const errors: ValidationErrors | null | undefined = this.registerForm.get("password")?.errors;

    return errors !== null;
  }

  passwordErrors(): string[] {

    const errorMessages: string[] = [];

    const errors: ValidationErrors | null | undefined = this.registerForm.get("password")?.errors;

    if (!errors) {
      return [];
    }

    if (errors['required']) {
      errorMessages.push("Password is required.");
    }

    if (errors['pattern']) {
      errorMessages.push("Password can cotain letters, numbers, dash and underscore.");
    }

    if (errors['minlength']){
      errorMessages.push("Username must be at least four characters long.");
    }

    return errorMessages;
  }

  passwordsNotMatching(): boolean {
    return this.registerForm.value.password !== this.registerForm.value.confirmPassword;
  }

  emailHasErrors(): boolean {

    const errors: ValidationErrors | null | undefined = this.registerForm.get("email")?.errors;

    return errors !== null;
  }

  emailErrors(): string[] {

    const errorMessages: string[] = [];

    const errors: ValidationErrors | null | undefined = this.registerForm.get("email")?.errors;

    if (!errors) {
      return [];
    }

    if (errors['required']) {
      errorMessages.push("Email is required.");
    }

    if (errors['email']) {
      errorMessages.push("Wrong email format.");
    }

    return errorMessages;
  }

  nameHasErrors(): boolean {

    const errors: ValidationErrors | null | undefined = this.registerForm.get("name")?.errors;

    return errors !== null;
  }

  nameErrors(): string[] {

    const errorMessages: string[] = [];

    const errors: ValidationErrors | null | undefined = this.registerForm.get("name")?.errors;

    if (!errors) {
      return [];
    }

    if (errors['required']) {
      errorMessages.push("Name is required.");
    }

    if (errors['pattern']) {
      errorMessages.push("Name can only cotain letters.");
    }

    return errorMessages;
  }

  lastNameHasErrors(): boolean {

    const errors: ValidationErrors | null | undefined = this.registerForm.get("lastName")?.errors;

    return errors !== null;
  }

  lastNameErrors(): string[] {

    const errorMessages: string[] = [];

    const errors: ValidationErrors | null | undefined = this.registerForm.get("lastName")?.errors;

    if (!errors) {
      return [];
    }

    if (errors['required']) {
      errorMessages.push("Last name is required.");
    }

    if (errors['pattern']) {
      errorMessages.push("Last name can only cotain letters.");
    }

    return errorMessages;
  }

  onRegisterFormSubmit() {
    const data = this.registerForm.value;
    console.log(data);

    if (this.registerForm.invalid) {
      return;
    }

    const obs: Observable<User | null> = this.auth.registerUser(data.username, data.password, data.email, data.name, data.lastName);
    this.sub = obs.subscribe((user: User | null) => {
      this.router.navigateByUrl('/');
    });
    
    this.registerForm.reset({
      username: '',
      password: '',
      confirmPassword: '',
      email: '',
      name: '',
      lastName: ''
    });
    
  }

}
