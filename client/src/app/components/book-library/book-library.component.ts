import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { BooksService } from 'src/app/services/books.service';
import { BookPagination } from '../../services/models/book-pagination';
import { Book } from '../../models/book.model';

declare const $: any;

@Component({
  selector: 'app-book-library',
  templateUrl: './book-library.component.html',
  styleUrls: ['./book-library.component.css']
})
export class BookLibraryComponent implements OnInit {

  private readonly elementsPerPage: number = 10;

  private bookPaginationStream: Subject<BookPagination>;
  public books: Book[] = [];

  public readonly genres: string[] = [
                                      'Action', 'Adventure','Art','Autobiography',
                                      'Biography','Cookbook','Classic','Comic','Crime',
                                      'Drama','Dictionary','Encyclopedia','Economics',
                                      'Fantasy','History','Horror','Hobbies','Health',
                                      'Humor','Journal','Mystery','Memoir','Poetry',
                                      'Philosophy','Romance','Sci-Fi', 'Satire','Sports',
                                      'Thriller','Travel','Western'
                                     ]

  public page: number = 1;
  public totalPages: number = 1;
  public hasPrevPage: boolean = false;
  public hasNextPage: boolean = false;

  public oldValueOfSearchBy: string = "title";
  public newValueOfSearchBy: string = "title";
  public searchedValue: string = "";
  private genre: string = "all";
  public selectedGenre: string = "all";

  constructor(private booksService: BooksService) {
    this.bookPaginationStream = this.booksService.getAllBooks(this.page, this.elementsPerPage);
  }

  ngOnInit(): void {
    this.bookPaginationStream.subscribe((bookPagination: BookPagination) => {
      this.books = bookPagination.docs;
      this.totalPages = bookPagination.totalPages;
      this.hasPrevPage = bookPagination.hasPrevPage;
      this.hasNextPage = bookPagination.hasNextPage;
    });

    $('.ui.dropdown').dropdown();
  }

  public clickSearchHandler(event: any): void {
    this.searchedValue = event.target.value;

    if (this.searchedValue.trim() == "") {
      return;
    }

    event.target.value = "";
    this.oldValueOfSearchBy = this.newValueOfSearchBy;
    this.getBooks();
  }

  public changePage(newPageNumber: number): void {
    this.page = newPageNumber;
    this.getBooks();
  }

  private getBooks(): void {
    if (this.genre == "all" && !this.searchedValue) {
      this.bookPaginationStream = this.booksService.getAllBooks(this.page, this.elementsPerPage);
    }
    else if (this.genre != "all" && this.searchedValue) {
      this.bookPaginationStream = this.booksService.getBooksBySearchedValueAndGenre(this.oldValueOfSearchBy, this.searchedValue, this.genre, this.page, this.elementsPerPage);
    }
    else if (this.genre != "all") {
      this.bookPaginationStream = this.booksService.getBooksByGenre(this.genre, this.page, this.elementsPerPage);
    }
    else if (this.oldValueOfSearchBy) {
      this.bookPaginationStream = this.booksService.getBooksBySearchedValue(this.oldValueOfSearchBy, this.searchedValue, this.page, this.elementsPerPage);
    }
  }

  public genreSelectChanged(event: any): void {
    this.genre = this.selectedGenre;
    this.getBooks();
  }

  public setValueOfSearchBy(event: any): void {
    this.newValueOfSearchBy = event.target.value;
  }

  public repealSearch(): void {
    this.searchedValue = "";
    this.getBooks();
  }

}
