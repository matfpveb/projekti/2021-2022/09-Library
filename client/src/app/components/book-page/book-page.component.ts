import { AfterContentChecked, Component, OnChanges, OnDestroy, OnInit } from '@angular/core';
import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable, Subscription, switchMap } from 'rxjs';
import { Book } from 'src/app/models/book.model';
import { User } from 'src/app/models/user.model';
import { Reservation } from 'src/app/models/reservation.model';
import { BooksService } from 'src/app/services/books.service';
import { AuthService } from 'src/app/services/auth.service';


declare const $: any;

@Component({
  selector: 'app-book-page',
  templateUrl: './book-page.component.html',
  styleUrls: ['./book-page.component.css']
})
export class BookPageComponent implements OnInit, OnDestroy {

  obs: Observable<Book> = new Observable<Book>();

  sub: Subscription = new Subscription();
  sub2: Subscription = new Subscription();
  sub3: Subscription = new Subscription();

  findIfReservationExistsSubscription: Subscription = new Subscription();
  changeNumberOfAvalableBooksSubscription: Subscription = new Subscription();
  createReservationSubscription: Subscription = new Subscription();

  book: Book = new Book('','','','',1,1,'','',1,1);
  user: User = new User('','','','','','','');

  reviewFormVisible: boolean = false;

  public reservationMsg: string = "";

  private readonly urls = {
    'getReservationUrl': "http://localhost:5000/api/reservations/reservation/",
    'changeBookNumberUrl': "http://localhost:5000/api/books/numberOfBooks/",
    'createReservationUlr': "http://localhost:5000/api/reservations/"
  };

  constructor(private bookService: BooksService,
              private activatedRoute: ActivatedRoute,
              private auth: AuthService,
              private http: HttpClient) { 
    
    this.user = this.auth.sendUserDataIfExists()!;

    this.sub = this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      const bookId: string | null = params.get('bookId');
      this.obs = this.bookService.getBookById(bookId!);
    });

    this.sub2 = this.obs.subscribe((book:Book) => {
      this.book = book;

      // Setup for rating after the book is initialised
      $('.ui.rating').rating('set rating', this.book.rating); 
      $('.ui.rating').rating('setting', 'onRate', (value: number) => {
        $('.ui.rating').rating('disable');

        const newRating = value == 1 ? Math.floor((this.book.rating + value)/2) : Math.ceil((this.book.rating + value)/2);
        this.sub3 = this.bookService.updateBookRating(this.book._id, newRating).subscribe((book: Book) => {
          this.book = book;

        });
      });
    });

  }

  changeReviewFormVisible(): void {
    this.reviewFormVisible = !this.reviewFormVisible;
  }

  ngOnInit(): void {

    $('.ui.rating').rating({
      maxRating: 5
    });

    $('.ui.rating').rating('enable');

    $('.ui.modal').modal();
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
    this.sub2.unsubscribe();
    this.sub3.unsubscribe();

    this.findIfReservationExistsSubscription.unsubscribe();
    this.changeNumberOfAvalableBooksSubscription.unsubscribe();
    this.createReservationSubscription.unsubscribe();
    
    $('.ui.modal').remove();
  }

  reserveBook(): void {
    
    const params: HttpParams = new HttpParams()
    .append('username', this.user.username.toString())
    .append('bookId', this.book._id.toString());
  
    const findIfReservationExists: Observable<Reservation> = this.http.get<Reservation>(this.urls.getReservationUrl, {params: params});
  
    this.findIfReservationExistsSubscription = findIfReservationExists.subscribe(
      (reservation: Reservation) => {
        this.reservationMsg = "Reservation already exists."
        $('.ui.modal').modal('show');
      },
      (error: HttpErrorResponse) => {
        if (error.status == 404) {
          this.createReservation(this.user.username.toString(), this.book._id.toString());
        }
      }
    );
  }

  private createReservation(username: string, bookId: string): void {

    const bookBody = {_id: bookId.toString(), number: "-1" };
    const reservationBody = {username: username.toString(), bookId: bookId.toString()};

    const changeNumberOfAvalableBooks: Observable<Book> = this.http.put<Book>(this.urls.changeBookNumberUrl, bookBody);
    const createReservation: Observable<Reservation> = this.http.post<Reservation>(this.urls.createReservationUlr, reservationBody);

    this.changeNumberOfAvalableBooksSubscription = changeNumberOfAvalableBooks.subscribe(
      (book: Book) => {
        this.book = book;
        this.createReservationSubscription = createReservation.subscribe(
          (reservation: Reservation) => {
            this.reservationMsg = "You have successfully reserved the book."
            $('.ui.modal').modal('show');
          },
          (error: any)  => {
            this.reservationMsg = "Reservation can't be placed. Try againg later."
            $('.ui.modal').modal('show');
          }
        );
      },
      (error: any) => {
        this.reservationMsg = "There are not enough books available."
        $('.ui.modal').modal('show');
      }
    );
  }
}
