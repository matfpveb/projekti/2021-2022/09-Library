import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';

declare const $: any;

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit, OnDestroy{

  loginForm: FormGroup;
  sub: Subscription = new Subscription();
  formHasErrors: boolean = false;

  constructor(private auth: AuthService,
              private router: Router) { 
    this.loginForm = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }

  ngOnInit(): void {
  
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();  
  }

  usernameHasErrors(): boolean {

    const errors: ValidationErrors | null | undefined = this.loginForm.get("username")?.errors;

    return errors !== null;
  }

  passwordHasErrors(): boolean {

    const errors: ValidationErrors | null | undefined = this.loginForm.get("password")?.errors;

    return errors !== null;
  }

  errorMessages(): string[] {

    const messages: string[] = [];

    if(this.usernameHasErrors()) {
      messages.push("Username required");
    }

    if(this.passwordHasErrors()) {
      messages.push("Password required");
    }

    return messages;
  }

  onLoginFormSubmit() {
    const data = this.loginForm.value;
    console.log(data);

    this.formHasErrors = true;

    const obs: Observable<User | null> = this.auth.loginUser(data.username,data.password);
    this.sub = obs.subscribe((user: User | null) => {
        this.router.navigateByUrl('/');
    });

    this.loginForm.reset({
      username: '',
      password: ''
    });
  }
}
