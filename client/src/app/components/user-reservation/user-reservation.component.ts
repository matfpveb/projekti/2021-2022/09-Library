import { Component, OnInit, Input } from '@angular/core';
import { Reservation } from 'src/app/models/reservation.model';
import { Book } from 'src/app/models/book.model';

@Component({
  selector: 'app-user-reservation',
  templateUrl: './user-reservation.component.html',
  styleUrls: ['./user-reservation.component.css']
})
export class UserReservationComponent implements OnInit {

  book: Book = new Book('123', 'Title', 'Author', 'Genre', 36, 4, 'Publisher', 'Description', 5, 1);
  @Input() reservation: Reservation = new Reservation("123", "unknown", "456", this.book, "status", "date");

  constructor() { }

  ngOnInit(): void {
  }

}
