import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBookWindowComponent } from './add-book-window.component';

describe('AddBookWindowComponent', () => {
  let component: AddBookWindowComponent;
  let fixture: ComponentFixture<AddBookWindowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddBookWindowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBookWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
