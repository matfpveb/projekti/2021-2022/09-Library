import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminReservationsListComponent } from './admin-reservations-list.component';

describe('AdminReservationsListComponent', () => {
  let component: AdminReservationsListComponent;
  let fixture: ComponentFixture<AdminReservationsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminReservationsListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminReservationsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
