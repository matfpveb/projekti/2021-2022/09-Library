import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { Reservation } from 'src/app/models/reservation.model';
import { ReservationsService } from 'src/app/services/reservations.service';
import { ReservationPagination } from 'src/app/services/models/reservation-pagination';
import { AuthService } from 'src/app/services/auth.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-admin-reservations-list',
  templateUrl: './admin-reservations-list.component.html',
  styleUrls: ['./admin-reservations-list.component.css']
})
export class AdminReservationsListComponent implements OnInit, OnDestroy {

  user: User = new User('','','','','','','');

  totalPages: number = 0;
  page: number = 0;
  hasPrevPage: boolean = false;
  hasNextPage: boolean = false;
  nextPage: number | null = 1;
  prevPage: number | null = 1;
  reservations: Reservation[] = [];
  sub: Subscription = new Subscription();
  subRev: Subscription = new Subscription();

  private readonly numOfElementsPerPage: number = 6;

  constructor(private auth: AuthService,
              private activatedRoute: ActivatedRoute,
              private reservationsService: ReservationsService) {

    this.user = this.auth.sendUserDataIfExists()!;

    this.sub = this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      const pageString: string = params.get("page")!;
      const pageNumber = parseInt(pageString);

      this.subRev = this.reservationsService.getAllReservations(pageNumber, this.numOfElementsPerPage, "date").subscribe((page: ReservationPagination) => {
        this.reservations = page.docs;
        this.page = page.page;
        this.totalPages = page.totalPages;
        this.hasNextPage = page.hasNextPage;
        this.hasPrevPage = page.hasPrevPage;
        this.prevPage = page.prevPage;
        this.nextPage = page.nextPage;
      });
    });
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.subRev.unsubscribe();
    this.sub.unsubscribe();
  }

  goToLastPage(): number {
    return this.totalPages;
  }

  goToNextPage(): number {

    if (!this.hasNextPage) {
      return this.page;
    }

    return this.nextPage!;
  }

  goToPrevPage(): number {

    if (!this.hasPrevPage) {
      return this.page;
    }

    return this.prevPage!;
  }

}
