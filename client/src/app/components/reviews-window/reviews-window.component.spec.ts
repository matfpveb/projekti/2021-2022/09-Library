import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewsWindowComponent } from './reviews-window.component';

describe('ReviewsWindowComponent', () => {
  let component: ReviewsWindowComponent;
  let fixture: ComponentFixture<ReviewsWindowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReviewsWindowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewsWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
