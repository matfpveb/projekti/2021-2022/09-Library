import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Book } from 'src/app/models/book.model';
import { BooksService } from 'src/app/services/books.service';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})
export class AddBookComponent implements OnInit, OnDestroy {

  genres: string[] = ['Action', 'Adventure','Art','Autobiography',
  'Biography','Cookbook','Classic','Comic','Crime',
  'Drama','Dictionary','Encyclopedia','Economics',
  'Fantasy','History','Horror','Hobbies','Health',
  'Humor','Journal','Mystery','Memoir','Poetry',
  'Philosophy','Romance','Sci-Fi', 'Satire','Sports',
  'Thriller','Travel','Western'];

  bookForm: FormGroup;
  sub: Subscription = new Subscription();

  constructor(private bookService: BooksService) { 

    this.bookForm = new FormGroup({
      title: new FormControl('',[Validators.required, Validators.pattern(new RegExp("[0-9a-zA-Z -!?,]{2,}"))]),
      author: new FormControl('',[Validators.required, Validators.pattern(new RegExp("[a-zA-Z -]{2,}"))]),
      genre: new FormControl(this.genres[0]),
      numberOfPages: new FormControl(1,[Validators.required, Validators.pattern(new RegExp("[0-9]{1,4}"))]),
      rating: new FormControl(5),
      booksAvailable: new FormControl(1,[Validators.required, Validators.pattern(new RegExp("[0-9]{1,3}"))]),
      publisher: new FormControl('',[Validators.required, Validators.pattern(new RegExp("[a-zA-Z -]{2,}"))]),
      description: new FormControl('',[Validators.required])
    });

  }

  ngOnInit(): void {

  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  titleHasErrors(): boolean {

    const errors: ValidationErrors | null | undefined = this.bookForm.get("title")?.errors;

    return errors !== null;
  }

  authorHasErrors(): boolean {

    const errors: ValidationErrors | null | undefined = this.bookForm.get("author")?.errors;

    return errors !== null;
  }

  publisherHasErrors(): boolean {

    const errors: ValidationErrors | null | undefined = this.bookForm.get("publisher")?.errors;

    return errors !== null;
  }

  numberOfPagesHasErrors(): boolean {

    const errors: ValidationErrors | null | undefined = this.bookForm.get("numberOfPages")?.errors;

    return errors !== null;
  }

  booksAvailableHasErrors(): boolean {

    const errors: ValidationErrors | null | undefined = this.bookForm.get("booksAvailable")?.errors;

    return errors !== null;
  }

  descriptionHasErrors(): boolean {

    const errors: ValidationErrors | null | undefined = this.bookForm.get("description")?.errors;

    return errors !== null;
  }

  onBookFormSubmit(): void {

    if (this.bookForm.invalid) {
      return;
    }

    const data = this.bookForm.value;

    this.sub = this.bookService.addBook(data.title, data.author, data.genre, data.numberOfPages, data.rating, data.publisher, data.description, data.booksAvailable).subscribe((book: Book) => {
      console.log(book);
    });

    this.bookForm.reset({
      title: '',
      author: '',
      genre: this.genres[0],
      numberOfPages: 1,
      rating: 5,
      booksAvailable: 1,
      publisher: '',
      description: ''
    });
  }
}
