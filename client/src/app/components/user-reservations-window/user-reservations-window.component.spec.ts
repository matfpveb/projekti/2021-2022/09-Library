import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserReservationsWindowComponent } from './user-reservations-window.component';

describe('UserReservationsWindowComponent', () => {
  let component: UserReservationsWindowComponent;
  let fixture: ComponentFixture<UserReservationsWindowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserReservationsWindowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserReservationsWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
