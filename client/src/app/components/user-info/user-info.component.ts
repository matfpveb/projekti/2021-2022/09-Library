import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';

declare const $: any;

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent implements OnInit, OnDestroy {

  changePasswordForm: FormGroup;
  changeEmailForm: FormGroup;
  changeImageForm: FormGroup;

  showPasswordError: boolean = false;
  showEmailError: boolean = false;


  subPassword: Subscription = new Subscription();
  subEmail: Subscription = new Subscription();
  subImage: Subscription = new Subscription();

  user: User | null = null;
  sub: Subscription = new Subscription();


  constructor(private userServ : UserService,
              private auth: AuthService) {

    this.sub = this.auth.user.subscribe((user: User | null) => {
      this.user = user;
    });

    this.user = this.auth.sendUserDataIfExists();

    this.changePasswordForm = new FormGroup({
      currentPassword: new FormControl('',[Validators.required]),
      newPassword: new FormControl('',[Validators.required, Validators.pattern(new RegExp("[0-9a-zA-Z_-]{8,}")),Validators.minLength(8)])
    });

    this.changeEmailForm = new FormGroup({
      email: new FormControl('',[Validators.required, Validators.email]),
      password: new FormControl('',[Validators.required])
    });

    this.changeImageForm = new FormGroup({
      imgUrl: new FormControl(''),
    });
  }

  ngOnInit(): void {
    
  }

  ngOnDestroy(): void {
    this.subPassword.unsubscribe();
    this.subEmail.unsubscribe();
    this.subImage.unsubscribe();
    this.sub.unsubscribe();
  }

  getImage(): string {

    if(!this.user) {
      return 'assets/default-user.png';
    }
    return this.user.getImageUrl();

  }

  cancelPasswordChange(): void {

    this.showPasswordError = false;

    this.changePasswordForm.reset({
      currentPassword: '',
      newPassword: ''
    });
  }

  cancelEmailChange(): void {

    this.showEmailError=false;

    this.changeEmailForm.reset({
      email: '',
      password: ''
    });
  }

  passwordsNotMatching(formPassword: string): boolean {
    return this.user?.password !== formPassword;
  }

  onChangePasswordSubmit(): void {
    const data = this.changePasswordForm.value;

    if (this.changePasswordForm.invalid || this.passwordsNotMatching(this.changePasswordForm.value.currentPassword)) {
      this.showPasswordError = true;
      return;
    }

    this.showPasswordError = false;

    const obs = this.userServ.patchUserPassword(this.changePasswordForm.value.currentPassword, this.changePasswordForm.value.newPassword);
    this.subPassword = obs.subscribe((user: User)=> {
      console.log(user);
    });

    this.changePasswordForm.reset({
      currentPassword: '',
      newPassword: ''
    });
  }

  onChangeEmailSubmit(): void {
    const data = this.changeEmailForm.value;

    if (this.changeEmailForm.invalid || this.passwordsNotMatching(this.changeEmailForm.value.password)) {
      this.showEmailError = true;
      return;
    }

    this.showEmailError=false;

    const obs = this.userServ.patchUserEmail(this.changeEmailForm.value.email, this.changeEmailForm.value.password);
    this.subEmail = obs.subscribe((user: User)=> {
      console.log(user);
    });

    this.changeEmailForm.reset({
      email: '',
      password: ''
    });
  }

  onFileChange(event: Event): void {
    const files: FileList | null = (event.target as HTMLInputElement).files;

    if(!files || files.length === 0) {
      return;
    }

    const imageForUpload = files[0];

    this.subImage = this.userServ.patchUserProfileImage(imageForUpload).subscribe((user: User) => {
      console.log(user);
    });

    this.changeImageForm.reset();
  }

}
