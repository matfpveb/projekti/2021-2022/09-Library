import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminReservationsWindowComponent } from './admin-reservations-window.component';

describe('AdminReservationsWindowComponent', () => {
  let component: AdminReservationsWindowComponent;
  let fixture: ComponentFixture<AdminReservationsWindowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminReservationsWindowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminReservationsWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
