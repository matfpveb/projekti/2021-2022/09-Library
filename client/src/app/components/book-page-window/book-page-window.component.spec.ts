import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookPageWindowComponent } from './book-page-window.component';

describe('BookPageWindowComponent', () => {
  let component: BookPageWindowComponent;
  let fixture: ComponentFixture<BookPageWindowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookPageWindowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookPageWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
