import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Book } from 'src/app/models/book.model';
import { Review } from 'src/app/models/review.model';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';
import { ReviewsService } from 'src/app/services/reviews.service';

@Component({
  selector: 'app-review-form',
  templateUrl: './review-form.component.html',
  styleUrls: ['./review-form.component.css']
})
export class ReviewFormComponent implements OnInit {

  @Input() book: Book = new Book('123','','','',1,1,'','',1,1);
  user: User = new User('','','','','','','');
  reviewForm: FormGroup;
  sub: Subscription = new Subscription();

  constructor(private auth: AuthService,
              private reviewsService: ReviewsService) {

    this.reviewForm = new FormGroup({
      title: new FormControl('',[Validators.required]),
      text: new FormControl('', [Validators.required])
    });

    this.user = this.auth.sendUserDataIfExists()!;
  }

  cancel(): void {
    this.reviewForm.reset({
      title: '',
      text: ''
    });
  }

  ngOnInit(): void {
  }

  onReviewFormSubmit(): void {

    if (this.reviewForm.invalid){
      return;
    }

    this.sub = this.reviewsService.postReview(this.reviewForm.value.title,this.reviewForm.value.text, this.user._id, this.book._id, new Date()).subscribe((review: Review) => {
      console.log(review);
    })

    this.reviewForm.reset({
      title: '',
      text: ''
    });
  }
}
