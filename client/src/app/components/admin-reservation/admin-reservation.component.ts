import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Reservation } from 'src/app/models/reservation.model';
import { Book } from 'src/app/models/book.model';
import { ReservationsService } from 'src/app/services/reservations.service';
import { Observable, Subscription } from 'rxjs';
import { ReservationPagination } from 'src/app/services/models/reservation-pagination';

@Component({
  selector: 'app-admin-reservation',
  templateUrl: './admin-reservation.component.html',
  styleUrls: ['./admin-reservation.component.css']
})
export class AdminReservationComponent implements OnInit, OnDestroy {

  readonly statuses: string[] = ['UNRESOLVED', 'APPROVED', 'REJECTED'];
  approveSub: Subscription = new Subscription();
  rejectSub: Subscription = new Subscription();

  book: Book = new Book('123', 'Title', 'Author', 'Genre', 36, 4, 'Publisher', 'Description', 5, 1);
  @Input() reservation: Reservation = new Reservation("123", "unknown", "456", this.book, "status", "date");

  constructor(private reservationsService: ReservationsService) { }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.approveSub.unsubscribe();
    this.rejectSub.unsubscribe();
  }

  approveReservation(): void {
    const approveObs: Observable<ReservationPagination> = this.reservationsService.changeReservationStatus(this.reservation._id, 'APPROVED')
    this.approveSub = approveObs.subscribe((reservation: ReservationPagination) => {
      this.reservation = reservation.docs[0];
    });
  }
}
