import { Component, OnInit, Input } from '@angular/core';
import { Book } from '../../models/book.model';

declare const $: any;

@Component({
  selector: 'app-book-card',
  templateUrl: './book-card.component.html',
  styleUrls: ['./book-card.component.css']
})
export class BookCardComponent implements OnInit {

  @Input() book!: Book;

  constructor() { }

  ngOnInit(): void {
    $('.ui.rating').rating({ maxRating: 5 });
    $('.ui.rating').rating('set rating', this.book.rating); 
    $('.ui.rating').rating('disable');
  }

  public getBookID() {
    return this.book._id;
  }

}
