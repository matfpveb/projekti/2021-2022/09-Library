import { Component, Input, OnInit } from '@angular/core';
import { Book } from 'src/app/models/book.model';
import { Review } from 'src/app/models/review.model';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit {

  user: User = new User('123','_sm_','mojasifra','mojemail@gmail.com','stefan','marjanovic','abc');
  book: Book = new Book('123','Naslov','Autor','Zanr',36,4,'Izdavac','Opis',3,1);

  @Input() review: Review = new Review('','','',this.user,this.book,'',1);

  constructor() { }

  getImage(): string {

    if(!this.review.user) {
      return 'assets/default-user.png';
    }
    return "http://localhost:5000/" + this.review.user.imgUrl;
  }


  ngOnInit(): void {
  }

}
