import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Subscription } from 'rxjs';
import { Review } from 'src/app/models/review.model';
import { ReviewPagination } from 'src/app/services/models/review-pagination';
import { ReviewsService } from 'src/app/services/reviews.service';

@Component({
  selector: 'app-reviews-list',
  templateUrl: './reviews-list.component.html',
  styleUrls: ['./reviews-list.component.css']
})
export class ReviewsListComponent implements OnInit, OnDestroy {

  totalPages: number = 0;
  page: number = 0;
  hasPrevPage: boolean = false;
  hasNextPage: boolean = false;
  nextPage: number | null = 1;
  prevPage: number | null = 1;
  reviews: Review[] = [];
  sub: Subscription = new Subscription();
  subRev: Subscription = new Subscription();


  constructor(private revService: ReviewsService,
              private activatedRoute: ActivatedRoute) {
                
    this.sub = this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      const pageString: string = params.get("page")!;
    
      const pageNumber = parseInt(pageString);

      this.subRev = this.revService.getReviews(pageNumber).subscribe((page: ReviewPagination) => {
        this.reviews = page.docs;
        this.page = page.page;
        this.totalPages = page.totalPages;
        this.hasNextPage = page.hasNextPage;
        this.hasPrevPage = page.hasPrevPage;
        this.prevPage = page.prevPage;
        this.nextPage = page.nextPage;

        console.log(this.reviews);
      });

    });

  }

  ngOnInit(): void {

  }

  ngOnDestroy(): void {
    this.subRev.unsubscribe();
    this.sub.unsubscribe();
  }

  goToLastPage(): number {
    return this.totalPages;
  }

  goToNextPage(): number {

    if (!this.hasNextPage) {
      return this.page;
    }

    return this.nextPage!;
  }

  goToPrevPage(): number {

    if (!this.hasPrevPage) {
      return this.page;
    }

    return this.prevPage!;
  }


}
