import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookLibraryWindowComponent } from './book-library-window.component';

describe('BookLibraryWindowComponent', () => {
  let component: BookLibraryWindowComponent;
  let fixture: ComponentFixture<BookLibraryWindowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookLibraryWindowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookLibraryWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
