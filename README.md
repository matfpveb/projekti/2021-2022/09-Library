# Project Library

Library je web aplikacija koja omogućava korisniku da rezerviše fizičko izdanje knjige koje preuzima u biblioteci. Korisniku je omogućeno da ostavi recenziju knjige, kao i da čita recenzije drugih korisnika.

# Required libraries

1. AngularJS
2. Node.js
3. MongoDB
4. Node Package Manager

# Build

1. Klonirati projekat izvršavanjem komande: `git clone https://gitlab.com/matfpveb/projekti/2021-2022/09-Library.git`
2. Pozicionirati se u direktorijum `09-Library/server` i komandom `npm install` instalirati potrebne pakete 
3. Pozicionirati se u direktorijum `09-Library/client` i komandom `npm install` instalirati potrebne pakete 
4. Pozicionirati se u direktorijum `09-Library/server` i komandom `npm start` pokrenuti serverski deo aplikacije (server)
5. Pozicionirati se u direktorijum `09-Library/client` i komandom `ng serve` pokrenuti klientski deo aplikacije (client)
6. U adresnu liniju web pregledača potrebno je uneti narednu adresu `http://localhost:4200/`

# Scheme

1. User - Sadrži podatke o korisnicima
2. Books - Sadrži podatke o knjigama
3. Reviews - Sadrži podatke o korisničkim recenzijama
4. Reservations - Sadrži podatke o korisničkim rezervacijama

# Demo video

Demo snimak možete pronaći klikom [ovde](https://www.youtube.com/watch?v=nG09EWeNKzM)

## Developers

- [Stefan Marjanović, 141/2017](https://gitlab.com/sm998)
- [Nikola Lazarević, 267/2017](https://gitlab.com/_nikolalazarevic)
