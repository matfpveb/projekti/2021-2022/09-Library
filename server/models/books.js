const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const bookSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    title: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    author: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    genre: {
        type: mongoose.Schema.Types.String,
        enum: {
            values: ['Action', 'Adventure','Art','Autobiography',
            'Biography','Cookbook','Classic','Comic','Crime',
            'Drama','Dictionary','Encyclopedia','Economics',
            'Fantasy','History','Horror','Hobbies','Health',
            'Humor','Journal','Mystery','Memoir','Poetry',
            'Philosophy','Romance','Sci-Fi', 'Satire','Sports',
            'Thriller','Travel','Western'],
            message: '{VALUE} is not supported'
        },
        required: true
    },
    number_of_pages: {
        type: mongoose.Schema.Types.Number,
        min: [1, 'Book must have at least one page'],
        max: 10000,
        required: true
    },
    rating: {
        type: mongoose.Schema.Types.Number,
        min: 1,
        max: 5,
        default: 5
    },
    publisher: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    description: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    books_available: {
        type: mongoose.Schema.Types.Number,
        min:0,
        max:100,
        required: true
    }
});

bookSchema.plugin(mongoosePaginate);

const bookModel = mongoose.model('books', bookSchema);

async function paginateThroughBooks(db_query, page = 1, limit = 10) {
    return await bookModel.paginate(db_query, {page, limit});
};

module.exports = {bookModel, paginateThroughBooks}