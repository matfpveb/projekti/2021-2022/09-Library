const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const reviewsSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    title: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    text: {
        type: mongoose.Schema.Types.String,
        required: true
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users',
        required: true
    },
    book: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'books',
        required: true
    },
    date: {
        type: mongoose.Schema.Types.Date,
        required: true
    }
});

reviewsSchema.plugin(mongoosePaginate);

const Review = mongoose.model('reviews', reviewsSchema);

async function paginateThroughReviews(page = 1, limit = 6) {
    return await Review.paginate({}, { page, limit, populate: ['user', 'book'], sort: '-date'});
}
  

module.exports = {Review, paginateThroughReviews}; 