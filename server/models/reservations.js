const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const reservationScheme = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    username: {
        type: mongoose.Schema.Types.String,
        require: true
    },
    bookId: {
        type: mongoose.Schema.Types.String,
        require: true
    }, 
    book: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'books',
        required: true
    },
    date: {
        type: mongoose.Schema.Types.Date,
        require: true
    },
    status: {
        type: mongoose.Schema.Types.String,
        enum : ['UNRESOLVED', 'APPROVED', 'REJECTED'], 
        default: 'UNRESOLVED',
        required: true
    }
});

reservationScheme.plugin(mongoosePaginate);

const reservationModel = mongoose.model('reservations', reservationScheme);

async function paginateThroughReservations(db_query, page, limit, sort_by) {
    return await reservationModel.paginate(db_query, {page, limit, populate: ['book'], sort: sort_by});
};

module.exports = { reservationModel, paginateThroughReservations };