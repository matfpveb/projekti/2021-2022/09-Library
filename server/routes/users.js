const express = require('express');
const usersController = require('../controllers/users');
const authencation = require('../utils/authentication');

const router = express.Router();

router.get('/', usersController.getAllUsers);

router.get('/:username', usersController.getUserByUsername);

router.post('/', usersController.addNewUser);

router.post('/register', usersController.registerUser);

router.post('/login', usersController.loginUser);

router.patch('/password', authencation.isAutencated ,usersController.changePassword);

router.patch('/email', authencation.isAutencated ,usersController.changeEmail);

router.patch('/profile-image', authencation.isAutencated ,usersController.changeProfileImage);

router.delete('/:username', usersController.deleteUserById);

module.exports = router;