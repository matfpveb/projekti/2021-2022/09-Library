const express = require('express');
const reviewsController = require('../controllers/reviews');

const router = express.Router();

router.get('/', reviewsController.getReviews);

router.get('/book', reviewsController.getReviewsForBook);

router.get('/:user', reviewsController.getReviewsForUser);

router.post('/', reviewsController.postReview);

module.exports = router;