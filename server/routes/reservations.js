const express = require('express');
const reservationsController = require('../controllers/reservations');

const router = express.Router();

router.get('/', reservationsController.getAllReservations);

router.get('/username', reservationsController.getAllUserReservationsByUsername);

router.get('/reservation', reservationsController.getReservationByUsernameAndBookId);

router.post('/', reservationsController.addNewReservation);

router.put('/status', reservationsController.changeReservationStatus);

router.delete('/', reservationsController.deleteReservation);

module.exports = router;