const express = require('express');
const booksController = require('../controllers/books');

const router = express.Router();

router.get('/', booksController.getAllBooks);

router.get('/search', booksController.getBooksBySearchedValue);

router.get('/genre', booksController.getBooksByGenre);

router.get('/search/genre', booksController.getBooksBySearchedValueAndGenre);

router.get('/title', booksController.getBookByTitle);

router.get('/author', booksController.getBooksByAuthor);

router.get('/:id', booksController.getBookById);

router.get('/genre', booksController.getBooksByGenre);

router.post('/', booksController.addNewBook);

router.put('/rating', booksController.updateBookRating);

router.put('/numberOfBooks', booksController.changeNumberOfAvailableBooks);

router.delete('/', booksController.deleteBook);


module.exports = router;