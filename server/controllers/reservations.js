const mongoose = require('mongoose');
const ReservationExport = require('../models/reservations.js');
const Reservation = ReservationExport.reservationModel;

const getAllReservations = async (req, res, next) => {
    const {page, limit, sort_by} = req.query;

    try {
        const allReservations = await ReservationExport.paginateThroughReservations({}, page, limit, sort_by);
        res.status(200).json(allReservations);
    } catch (error) {
        next(error);
    }
}

const getAllUserReservationsByUsername = async (req, res, next) => {
    const {username, page, limit, sort_by} = req.query;

    try {
        if (!username) {
            const error = new Error("Username is missing!");
            error.status = 400;
            throw error;
        }

        const userReservations = await ReservationExport.paginateThroughReservations({ 'username': username }, page, limit, sort_by);
        if (userReservations) {
            res.status(200).json(userReservations);
        } else {
            res.status(404).json();
        }
    } catch (error) {
        next(error);
    }
};

const getReservationByUsernameAndBookId = async (req, res, next) => {
    const {username, bookId} = req.query;
    
    try {
        if (!username || !bookId) {
            const error = new Error("Missing information!");
            error.status = 400;
            throw error;
        }

        const userReservation = await Reservation.findOne({username: username, bookId: bookId}).exec();
        if (userReservation) {
            res.status(200).json(userReservation);
        } else {
            res.status(404).json();
        }
    } catch (error) {
        next(error);
    }
};

const addNewReservation = async (req, res, next) => {
    const {username, bookId}  = req.body;

    try {
        if (!username || !bookId) {
            const error = new Error("Missing information!");
            error.status = 400;
            throw error;
        }

        const newReservation  = new Reservation({
            _id: mongoose.Types.ObjectId(),
            username: username,
            bookId: bookId,
            book: bookId,
            date: new Date()
        });

        await newReservation.save();
        res.status(201).json(newReservation);
    } catch (error) {
        next(error);
    }
};

const changeReservationStatus = async (req,res,next) => {
    const {_id, status} = req.body;

    try {

        if(!_id || !status){
            const error = new Error("Missing information!");
            error.status = 400;
            throw error;
        }

        const reservation = await Reservation.findOne({_id:_id}).exec();

        if(!reservation){
            res.status(404).json();
        }
        else {
            const updateReservation = await Reservation.updateOne({_id: _id}, {$set: { status: status }}).exec();
            const reservationUpdated = await ReservationExport.paginateThroughReservations({'_id': _id}, 1, 1, "status");
            res.status(200).json(reservationUpdated);
        }
        
    } catch(error) {
        next(error);
    }
}

const deleteReservation = async (req, res, next) => {
    const {id} = req.body;
    
    try {
        if(!id) {
            const error = new Error("Missing information!");
            error.status = 400;
            throw error;
        }

        const reservationDeleted = await Reservation.deleteOne({_id: id}).exec();

        if(reservationDeleted.deletedCount == 1) {
            res.status(200).json({success: true});
        } else {
            res.status(404).json();
        }

    } catch(error) {
        next(error);
    }
};

module.exports = {
    getAllReservations,
    getAllUserReservationsByUsername,
    getReservationByUsernameAndBookId,
    addNewReservation,
    changeReservationStatus,
    deleteReservation
}