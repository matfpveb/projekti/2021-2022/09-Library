const mongoose = require('mongoose');
const BookExport = require('../models/books.js');
const Book = BookExport.bookModel;


/*const getAllBooks = async (req,res,next) => {

    try {
        const allBooks = await Book.find({}).exec();
        res.status(200).json(allBooks);
    } catch(error) {
        next(error);
    }
};*/

const getBookByTitle = async (req,res,next) => {
    const {title} = req.body;

    try {

        if (!title) {
            const error = new Error("Book title is missing!");
            error.status = 400;
            throw error;
        }

        const reg = new RegExp('.*\\b'+title+'.*','i');
        const book = await Book.findOne({title: {$regex: reg}}).exec();
        if(book) {
            res.status(200).json(book);
        } else {
            res.status(404).json();
        }

    } catch(error) {
        next(error);
    }

};

const getBooksByAuthor = async (req,res,next) => {
    const {author} = req.body;

    try {

        if (!author) {
            const error = new Error("Book author is missing!");
            error.status = 400;
            throw error;
        }

        const reg = new RegExp('.*\\b'+author+'.*','i');
        const books = await Book.find({author: {$regex: reg}}).exec();

        res.status(200).json(books);

    } catch(error) {
        next(error);
    }

};

const getBookById = async (req,res,next) => {
    const id = req.params.id;

    try {

        if (!id) {
            const error = new Error("Book id is missing!");
            error.status = 400;
            throw error;
        }

        const book = await Book.findOne({_id: id}).exec();
        if(book) {
            res.status(200).json(book);
        } else {
            res.status(404).json();
        }

    } catch(error) {
        next(error);
    }

};
/*
const getBooksByGenre = async (req,res,next) => {
    const {genre} = req.body;

    try {

        if (!genre) {
            const error = new Error("Book genre is missing!");
            error.status = 400;
            throw error;
        }

        const genres = ['Action', 'Adventure','Art','Autobiography',
        'Biography','Cookbook','Classic','Comic','Crime',
        'Drama','Dictionary','Encyclopedia','Economics',
        'Fantasy','History','Horror','Hobbies','Health',
        'Humor','Journal','Mystery','Memoir','Poetry',
        'Philosophy','Romance','Sci-Fi', 'Satire','Sports',
        'Thriller','Travel','Western'];

        if (!genres.includes(genre)){
            const error = new Error("Wrong genre!");
            error.status = 400;
            throw error;
        }

        const books = await Book.find({genre: genre}).exec();
        res.status(200).json(books);

    } catch (error) {
        next(error);
    }

}
*/

const addNewBook = async (req,res,next) => {
    const {title,author,genre,number_of_pages, 
        publisher, description, books_available} = req.body;

    try {
        if (!title || !author || !genre || !number_of_pages 
            || !publisher || !description || !books_available) {

            const error = new Error("Missing information!");
            error.status = 400;
            throw error;
        }

        const newBook = new Book({
            _id: mongoose.Types.ObjectId(),
            title: title,
            author: author,
            genre: genre,
            number_of_pages: number_of_pages,
            publisher: publisher,
            description: description,
            books_available: books_available
        });

        await newBook.save();
        res.status(201).json(newBook);

    } catch(error) {
        next(error);
    }

};

const updateBookRating = async (req,res,next) => {
    const {id, newRating} = req.body;

    try {

        if (!id || newRating==undefined) {
            const error = new Error("Missing information!");
            error.status = 400;
            throw error;
        }

        if(newRating < 1 || newRating > 5){
            const error = new Error("Rating must be between 1 and 5!");
            error.status = 400;
            throw error;
        }

        const bookUpdated = await Book.updateOne({_id: id}, {rating: newRating}).exec();
        if(bookUpdated.matchedCount<1) {
            res.status(404).json();
        } else {
            const book = await Book.findOne({_id: id}).exec();
            res.status(200).json(book);
        }


    } catch(error){
        next(error);
    }

}

const changeNumberOfAvailableBooks = async (req,res,next) => {
    const {_id, number} = req.body;

    try {

        if(!_id || !number){
            const error = new Error("Missing information!");
            error.status = 400;
            throw error;
        }

        const book = await Book.findOne({_id:_id}).exec();

        if(!book){
            res.status(404).json();
        } else if ((book.books_available + Number(number)) < 0 ){
            const error = new Error("Number of books must be greater than zero!");
            error.status = 400;
            throw error;
        } else {
            const updateBook = await Book.updateOne({_id: _id},{$inc: {books_available: number}}).exec();
            const bookUpdated = await Book.findOne({_id:_id}).exec();
            res.status(200).json(bookUpdated);
        }
        
    } catch(error) {
        next(error);
    }
}

const deleteBook = async (req,res,next) => {
    const {id} = req.body;
    
    try {
        if(!id) {
            const error = new Error("Missing information!");
            error.status = 400;
            throw error;
        }

        const bookDeleted = await Book.deleteOne({_id: id}).exec();

        if(bookDeleted.deletedCount == 1) {
            res.status(200).json({success: true});
        } else {
            res.status(404).json();
        }

    } catch(error) {
        next(error);
    }
};

const getAllBooks = async function (req, res, next) {
    const { page, limit} = req.query;
    
    try {
        const books = await BookExport.paginateThroughBooks({}, page, limit);
        res.status(200).json(books);
    } catch (error) {
        next(error);
    }
}

const getBooksBySearchedValue = async function (req, res, next) {
    const { searchBy, searchValue, page, limit} = req.query;

    try {
        const books = await BookExport.paginateThroughBooks({ [searchBy]: searchValue }, page, limit);
        res.status(200).json(books);
    } catch (error) {
        next(error);
    }
}

const getBooksByGenre = async function (req, res, next) {
    const { genre, page, limit} = req.query;

    try {
        const books = await BookExport.paginateThroughBooks({ 'genre': genre }, page, limit);
        res.status(200).json(books);
    } catch (error) {
        next(error);
    }
}

const getBooksBySearchedValueAndGenre = async function (req, res, next) {
    const { searchBy, searchValue, genre, page, limit } = req.query;
    
    try {
        const books = await BookExport.paginateThroughBooks(
        {
            [searchBy]: searchValue,
            'genre': genre
        }, page, limit);
        res.status(200).json(books);
    } catch (error) {
        next(error);
    }
}

module.exports = {
    getBookByTitle,
    getBooksByAuthor,
    getBookById,
    addNewBook,
    updateBookRating,
    changeNumberOfAvailableBooks,
    deleteBook,
    getAllBooks,
    getBooksBySearchedValue,
    getBooksByGenre,
    getBooksBySearchedValueAndGenre
}
