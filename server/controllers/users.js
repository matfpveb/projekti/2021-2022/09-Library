const { json } = require('body-parser');
const mongoose = require('mongoose');
const User = require('../models/users.js');
const jwtUtil = require('../utils/jwt')
const {uploadFile} = require('./uploadController');

const getAllUsers = async (req, res, next) => {
    try {
        const allUsers = await User.find({}).exec();
        res.status(200).json(allUsers);
    } catch (error) {
        next(error);
    }
};

const getUserByUsername = async (req, res, next) => {
    const username = req.params.username;

    try {
        if (username == undefined){
            const error = new Error('Username missing!');
            error.status = 400;
            throw error;
        }

        const user = await User.findOne({username: username}).exec();
        if (user){
            res.status(200).json(user);
        } else {
            res.status(404).json();
        }

    } catch (error) {
        next(error);
    }
};

const loginUser = async (req, res, next) => {
    const {username, password} = req.body;

    try {
        if (username == undefined || !password){
            const error = new Error('Username or password missing!');
            error.status = 400;
            throw error;
        }

        const user = await User.findOne({username: username}).exec();
        if (!user){
            const error = new Error(`User with username ${username} does not exist!`);
            error.status = 404;
            throw error;
        } else if (password !== user.password){
            const error = new Error('Wrong password!');
            error.status = 401;
            throw error;
        } else {
            const jwt = jwtUtil.generateJWT({
                id: user._id,
                username: user.username,
                password: user.password,
                email: user.email,
                name: user.name,
                lastName: user.lastName,
                imgUrl: user.imgUrl
            });

            res.status(200).json({token: jwt});
        }

    } catch (error) {
        next(error);
    }
};


const addNewUser = async (req, res, next) => {
    const {username, password, email, name, lastName} = req.body;

    try {
        if (!username || !password || !email || !name || !lastName) {
            const error = new Error('Missing information!');
            error.status = 400;
            throw error;
        }
        
        const user = await User.findOne({username:username}).exec();
        if (user) {
            const error = new Error('Username already taken!');
            error.status = 403;
            throw error;
        }
        
        const userEmail = await User.findOne({email:email}).exec();
        if (userEmail) {
            const error = new Error('Email already in use!');
            error.status = 403;
            throw error;
        }

        const newUser = User({
            _id: mongoose.Types.ObjectId(),
            username: username,
            password: password,
            email: email,
            name: name,
            lastName: lastName
        });

        await newUser.save();
        res.status(201).json(newUser);

    } catch (error) {
        next(error);        
    }
};

const registerUser = async (req, res, next) => {
    const {username, password, email, name, lastName} = req.body;

    try {
        if (!username || !password || !email || !name || !lastName) {
            const error = new Error('Missing information!');
            error.status = 400;
            throw error;
        }
        
        const user = await User.findOne({username:username}).exec();
        if (user) {
            const error = new Error('Username already taken!');
            error.status = 403;
            throw error;
        }
        
        const userEmail = await User.findOne({email:email}).exec();
        if (userEmail) {
            const error = new Error('Email already in use!');
            error.status = 403;
            throw error;
        }

        const newUser = User({
            _id: mongoose.Types.ObjectId(),
            username: username,
            password: password,
            email: email,
            name: name,
            lastName: lastName
        });

        await newUser.save();

        const jwt = jwtUtil.generateJWT({
            id: newUser._id,
            username: newUser.username,
            password: newUser.password,
            email: newUser.email,
            name: newUser.name,
            lastName: newUser.lastName,
            imgUrl: newUser.imgUrl
        });

        res.status(201).json({token: jwt});

    } catch (error) {
        next(error);        
    }
};


const changePassword = async (req, res, next) => {
    const { oldPassword, newPassword} = req.body;
    const username = req.username;

    try {

        if (!username || !oldPassword || !newPassword) {
            const error = new Error('Missing information!');
            error.status = 400;
            throw error;
        }

        const user = await User.findOne({username:username}).exec();
        if(!user) {
            res.status(404).json();
        }
        else if(oldPassword != user.password){
            error = new Error('New and old password not matching!');
            error.status = 403;
            throw error;
        } else {
            const userUpdated = await User.updateOne({username:username},{password: newPassword}).exec();    
            const user = await User.findOne({username:username}).exec();
        
            const jwt = jwtUtil.generateJWT({
                id: user._id,
                username: user.username,
                password: user.password,
                email: user.email,
                name: user.name,
                lastName: user.lastName,
                imgUrl: user.imgUrl
            });
    
            res.status(201).json({token: jwt});
        }

    } catch(error) {
        next(error);
    }
    
};

const changeEmail = async (req, res, next) => {
    const {password, email} = req.body;
    const username = req.username;

    try {

        if(!username || !password ||!email) {
            const error = new Error('Missing information!');
            error.status = 400;
            throw error;
        }

        const user = await User.findOne({username:username}).exec();
        
        if(!user) {
            res.status(404).json();
        } else if (password != user.password){
            error = new Error('Wrong password!');
            error.status = 403;
            throw error;
        } else {
            await User.updateOne({username: username}, {email: email}).exec();
            const userUpdated = await User.findOne({username:username}).exec();

            const jwt = jwtUtil.generateJWT({
                id: userUpdated._id,
                username: userUpdated.username,
                password: userUpdated.password,
                email: userUpdated.email,
                name: userUpdated.name,
                lastName: userUpdated.lastName,
                imgUrl: userUpdated.imgUrl
            });
    
            res.status(201).json({token: jwt});
        }

    } catch(error) {
        next(error);
    }

}

const changeProfileImage = async (req,res,next) => {
    const username = req.username;
    const userId = req.userId;

    try {

        const user = await User.findOne({username: username}).exec();

        if (!user) {
            const error = new Error('Username missing!');
            error.status = 400;
            throw error;
        }

        await uploadFile(req,res);
        console.log(req.file);

        if (req.file == undefined) {
            const error = new Error('File missing!');
            error.status = 400;
            throw error;
        }

        const imgUrl = req.file.filename;
        const updateUser = await User.updateOne({username: username}, {imgUrl: imgUrl}).exec();
        const userUpdated = await User.findOne({username:username}).exec();

        const jwt = jwtUtil.generateJWT({
            id: userUpdated._id,
            username: userUpdated.username,
            password: userUpdated.password,
            email: userUpdated.email,
            name: userUpdated.name,
            lastName: userUpdated.lastName,
            imgUrl: userUpdated.imgUrl
        });

        res.status(201).json({token: jwt});

    } catch (error) {
        next(error);
    }
}

const deleteUserById = async (req,res,next) => {
    const username = req.params.username;

    try {
        if (username == undefined) {
            const error = new Error('Username missing!');
            error.status = 400;
            throw error;
        }

        const userDelted = await User.deleteOne({username: username}).exec();

        if(userDelted.deletedCount == 1) {
            res.status(200).json({success: true});
        } else {
            res.status(404).json();
        }

    } catch (error) {
        next(error);
    }
}

module.exports = {
    getAllUsers,
    getUserByUsername,
    loginUser,
    addNewUser,
    registerUser,
    deleteUserById,
    changePassword,
    changeEmail,
    changeProfileImage
};