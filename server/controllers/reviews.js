const mongoose = require('mongoose');
const {Review, paginateThroughReviews} = require('../models/reviews');

const getReviews = async (req,res,next) => {
    const page = req.query.page;
    const limit = req.query.limit;
  
    try {
        const allReviews = await paginateThroughReviews(page, limit);
        res.status(200).json(allReviews);
    } catch (error) {
        next(error);
    }
};

const getReviewsForUser = async (req,res,next) => {
    const user = req.params.user;

    try {
        if(!user){
            const error = new Error('Username missing!');
            error.status = 400;
            throw error;
        }

        const reviews= await Review.find({user:user}).exec();
        res.status(200).json(reviews);

    } catch (error) {
        next(error);
    }
};

const getReviewsForBook = async (req,res,next) => {
    const {book_id} = req.body;

    try {
        if(!book_id){
            const error = new Error('Book id missing!');
            error.status = 400;
            throw error;
        }

        const reviews = await Review.find({book_id: book_id}).exec();
        res.status(200).json(reviews);

    } catch (error) {
        next(error);
    }
};

const postReview = async (req,res,next) => {
    const {title, text, user, book, date} = req.body;

    try {
        if(!title || !text || !user || !book || !date){
            const error = new Error('Missing information!');
            error.status = 400;
            throw error;
        }

        const newReview = new Review({
            _id: mongoose.Types.ObjectId(),
            title: title,
            text: text,
            user: user,
            book: book,
            date: date
        });

        await newReview.save();
        res.status(201).json(newReview);

    } catch (error) {
        next(error);
    }
};

module.exports = {
    getReviews,
    getReviewsForUser,
    getReviewsForBook,
    postReview
};