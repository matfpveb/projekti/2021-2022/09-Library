const express = require('express');
const jwt = require('./jwt');
const User = require('../models/users');

module.exports.isAutencated = (req,res,next) => {

    try {

        const authHeader = req.header("Authorization");
        if (!authHeader) {
            const error = new Error("Authorization header missing!");
            error.status = 403;
            throw(error);
        }

        const token = authHeader.split(' ')[1];
        const decodedToken = jwt.verifyJWT(token);
        if (!decodedToken) {
            const error = new Error('Not authenticated!');
            error.status = 401;
            throw(error);
        }

        req.username = decodedToken.username;
        req.userId = decodedToken.id;

        next();
    } catch(error) {
        next(error);
    }

}