const jwt = require('jsonwebtoken');
const MY_SECRET = 'OUR_LITTLE_SECRET';

// data is payload
module.exports.generateJWT = (data) => {
    // Returns JWT as string
    return jwt.sign(data, MY_SECRET, {expiresIn: '10d'});
}

// token is JWT string
module.exports.verifyJWT = (token) => {
    // Returns decoded payload (or error if signature is not valid, token is expired..)
    return jwt.verify(token, MY_SECRET);
}