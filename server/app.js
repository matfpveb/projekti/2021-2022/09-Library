// Creating directory for resources (if it doesn't exist)
const path = require('path');
const fs = require('fs');
global.__uploadDir = path.join(__dirname, 'resources', 'uploads');
if (!fs.existsSync(__uploadDir)) {
  fs.mkdirSync(__uploadDir, { recursive: true });
}

const express = require('express');
const {json, urlencoded} = require('body-parser');
const mongoose = require('mongoose');

const usersRouter = require('./routes/users');
const bookRouter = require('./routes/books');
const reviewsRouter = require('./routes/reviews');
const reservationsRouter = require('./routes/reservations');

const app = express();

const databaseString = process.env.DB_STRING || 'mongodb://localhost:27017/library_db';

mongoose.connect(databaseString, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

mongoose.connection.once('open', function() {
    console.log('Connected to database succesfully!');
});

mongoose.connection.on('error', function() {
    console.log('Connection error', error);
});

app.use(json());
app.use(urlencoded({extended: false}));

app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  
    if (req.method === 'OPTIONS') {
      res.header('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PATCH, PUT, DELETE');
  
      return res.status(200).json({});
    }
  
    next();
});  

app.use('/api/users',usersRouter);
app.use('/api/books',bookRouter);
app.use('/api/reviews',reviewsRouter);
app.use('/api/reservations',reservationsRouter);

// For image requests
app.use(express.static(__uploadDir));

// Handling unsupported requests 
app.use(function (req, res, next) {
    const error = new Error('Request is not supported!');
    error.status = 405;

    next(error);
});

// Error handling
app.use(function (error, req, res, next) {
    const statusCode = error.status || 500;
    res.status(statusCode).json({
        error: {
            message: error.message,
            status: statusCode,
            stack: error.stack,
        }
    });
});

module.exports = app;